//    Crafty.e("2D, DOM, Text")
//      .attr({x: pos * 36, y: 19 * 36})
//      .text(msg.data.split(" ")[1])
//      .css({"font-size" : "8px"});

window.onload = (function() {

	// Game variables
	var TILE_SIZE = 40,
		BOARD_INSET = 200,
		BOARD_ROWS = 14,
		BOARD_COLS = 24,
		HEIGHT = TILE_SIZE*BOARD_ROWS,
		WIDTH = TILE_SIZE*BOARD_COLS,
		FONT = "SyntaxError";

	// Crafty initialization
    Crafty.init(WIDTH, HEIGHT, 30);
	Crafty.canvas.init();

	// Tile sprites
	Crafty.sprite(TILE_SIZE, "images/tiles.png", {
		Tile1:  [0, 0],	// A
		Tile2:  [1, 0],	// B
		Tile3:  [2, 0],	// C
		Tile4:  [3, 0],	// D
		Tile5:  [4, 0],	// E
		Tile6:  [5, 0],	// F
		Tile7:  [6, 0],	// G
		Tile8:  [7, 0],	// H
		Tile9:  [8, 0],	// I
		Tile10: [0, 1],	// J
		Tile11: [1, 1],	// K
		Tile12: [2, 1],	// L
		Tile13: [3, 1],	// M
		Tile14: [4, 1],	// N
		Tile15: [5, 1],	// O
		Tile16: [6, 1],	// P
		Tile17: [7, 1],	// Q
		Tile18: [8, 1],	// R
		Tile19: [0, 1],	// S
		Tile20: [1, 2],	// T
		Tile21: [2, 2],	// U
		Tile22: [3, 2],	// V
		Tile23: [4, 2],	// W
		Tile24: [5, 2],	// X
		Tile25: [6, 2],	// Y
		Tile26: [7, 2],	// Z
		//Tile0:  [8, 2]	// Blank
	});

//*************** COMPONENTS ***************//
	// Player 1 Controls Component
	Crafty.c("P1Controls", {_keys: { 
   		S: [0,1],
    	D: [1,0],
    	A: [-1,0],
    }, 

    init: function() {
      for(var k in this._keys) {
        var keyCode = Crafty.keys[k] || k;
        this._keys[keyCode] = this._keys[k];
      }

      this.bind("KeyDown",function(e) {
        if(this._keys[e.key]) {
          var direction = this._keys[e.key];
          this.trigger('Slide',direction);
        }
      })
    } // end init
  	}); // end P1Controls

  // Slide Component
  Crafty.c("Slide", {
    init: function() {
      this._stepFrames = 5;
      this._moving = false;
      this._vx = 0; this._destX = 0; this._sourceX = 0;
      this._vy = 0; this._destY = 0; this._sourceY = 0;
      this._frames = 0;

      this.bind("Slide", function(direction) {
        // Don't continue to slide if we're already moving
        if(this._moving) return false;
        this._moving = true;

        // Let's keep our pre-movement location
        this._sourceX = this.x;
        this._sourceY = this.y;

        // Figure out our destination
        this._destX = this.x + direction[0] * TILE_SIZE;
        this._destY = this.y + direction[1] * TILE_SIZE;

        // Get our x and y velocity
        this._vx = direction[0] * TILE_SIZE / this._stepFrames;
        this._vy = direction[1] * TILE_SIZE / this._stepFrames;

        this._frames = this._stepFrames;
      }).bind("EnterFrame",function(e) {
        if(!this._moving) return false;

        // If we're moving, update our position by our per-frame velocity
        this.x += this._vx;
        this.y += this._vy;
        this._frames--;

        if(this._frames == 0) {
          // If we've run out of frames,
          // move us to our destination to avoid rounding errors.
          this._moving = false;
          this.x = this._destX;
          this.y = this._destY;
        }
        this.trigger('Moved', {x: this.x, y: this.y});
      });

    }, 
    slideFrames: function(frames) { 
       this._stepFrames = frames;
    },

    // A function we'll use later to 
    // cancel our movement and send us back to where we started
    cancelSlide: function() {
      this.x = this._sourceX;
      this.y = this._sourceY;
      this._moving = false;
    }
  });

	// Tile Component  
  	Crafty.c("Tile", {      
       init: function() {
          this.addComponent("2D, Canvas, Slide, P1Controls");
          this.w = TILE_SIZE;
          this.h = TILE_SIZE;

		  // Really should be a for loop
          var tileGridSnaps = [0, TILE_SIZE, TILE_SIZE*2, TILE_SIZE*3, TILE_SIZE*4, TILE_SIZE*5,
                               TILE_SIZE*6, TILE_SIZE*7, TILE_SIZE*8, TILE_SIZE*9, TILE_SIZE*10,
                               TILE_SIZE*11, TILE_SIZE*12, TILE_SIZE*13, TILE_SIZE*14];

          var startX = (BOARD_INSET + Crafty.math.randomElementOfArray(tileGridSnaps));
		  this.attr({x:startX, y:-TILE_SIZE, w:TILE_SIZE, h:TILE_SIZE});
		  
		  this.addComponent('Gravity').gravity("Bottom").gravityConst(0.020);

		  this.addComponent("Collision")
			.onHit("Divider",function(obj) {
     			this.cancelSlide();
	 		})
			.onHit("Tile",function(obj) {
                this.antigravity();
                this.removeComponent("P1Controls", false);
			})
			.onHit("Floor",function() {
				this.removeComponent("P1Controls", false);
			});
       },       
  	});

	// Game Board Component
    Crafty.c("Board", {

        // Point values for each tile
        //POINTS: ["0", "1", "3", "3", "2", "1", "4", "2", "4", "1", "8", "5", "1", "3", "1", "1", "3", "10", "1", "1", "1", "1", "4", "8", "4", "10"],
		POINTS: ["1", "3", "3", "2", "1", "4", "2", "4", "1", "8", "5", "1", "3", "1", "1", "3", "10", "1", "1", "1", "1", "4", "8", "4", "10"],

        // Initialize board layout
        init: function() {
            this.addComponent("2D, Canvas, Color");
            this.x = BOARD_INSET;
            this.w = WIDTH;
            this.h = HEIGHT;
            this.color("#CCCCCC");
			
			// Used for hit detection to remove controls
			this.floor = Crafty.e("2D, Canvas, Collision, Floor")
				.attr({w:Crafty.viewport.width, y:HEIGHT-1});
	
			// Used for stopping tile based upon gravity component
			this.bottomDiv = Crafty.e("2D, Canvas, Collision, Divider, Bottom")
				.attr({w:Crafty.viewport.width, y:HEIGHT});

			this.leftDiv = Crafty.e("2D, Canvas, Collision, Divider")
				.attr({x: BOARD_INSET, h:Crafty.viewport.height});

			this.rightDiv = Crafty.e("2D, Canvas, Collision, Divider")
				.attr({x: WIDTH, h:Crafty.viewport.height});
     	}
	 }); // end Board

 	 Crafty.c("Panel", {

        // Initialize panel layout
        init: function() {
            this.addComponent("2D, Canvas, Color");
            this.w = BOARD_INSET;
            this.h = HEIGHT;
            this.color("#000000");

			var score = 0;
            this.scoreLabel = Crafty.e("2D, Canvas, SpriteText")
                 .attr({y: 75, w: BOARD_INSET, h: TILE_SIZE})
				 .align("center")
                 .registerFont(FONT, 32, "images/font_syntax-terror.png")
                 .text("Score");    

			this.scoreEnt = Crafty.e("2D, Canvas, SpriteText")
                 .attr({y: 125, w: BOARD_INSET, h: TILE_SIZE})
				 .align("center")
                 .font(FONT)
                 .text(score);			        
     	}
	 }); // end Panel

//*************** SCENES ***************//
Crafty.scene("Game", function() {
	Crafty.e("Panel"); // init score panel
    Crafty.e("Board"); // init game field

	// Pause when user leaves game tab
	Crafty.settings.register("AutoPause");

    var tiles = new Array(); // array of all tiles

	Crafty.bind("EnterFrame", function() {

		// Game Loop -- Are any entities being controlled?
		if(Crafty("P1Controls").length < 1){

		  var tileNum = Crafty.math.randomInt(1,26); // Randomly choose a tile sprite
	      var currTile = Crafty.e("2D, Canvas, P1Controls, Tile, Tile" + tileNum);
		  tiles.push(currTile);
		}//end game loop
    }); //end EnterFrame
}); // end Game Scene

    // Call loading scene
    Crafty.scene("Loading"); 
    
}); // end window.onload
