// Loading Screen
Crafty.scene("Loading", function() {
    Crafty.load(["images/tiles.png", "images/font_syntax-terror.png"], function() {
        Crafty.scene("Game"); // Start game
    });
});


// Game Over / Restart Screen
Crafty.scene("Restart", function() {
    var vcenter = HEIGHT / 2,
        bg = Crafty.e("2D, Canvas, Color, Mouse")
            .attr({w: WIDTH, h: HEIGHT})
            .color("#000");

    Crafty.e("2D, Canvas, SpriteText")
		.attr({x: BOARD_LEFT, y: vcenter - 90, w: width, h: 32})
		.registerFont(FONT, 32, "images/font_syntax-terror.png")
		.align("center")
		.text("Your Score is");
    Crafty.e("2D, Canvas, SpriteText")
		.attr({x: BOARD_LEFT, y: vcenter - 25, w: width, h: 32})
		.font(FONT)
		.align("center")
		.text(score);
    Crafty.e("2D, Canvas, SpriteText")
		.attr({x: BOARD_LEFT, y: vcenter + 40, w: width, h: 32})
		.font(FONT)
		.align("center")
		.text("Click");
    Crafty.e("2D, Canvas, SpriteText")
		.attr({x: BOARD_LEFT, y: vcenter + 80, w: width, h: 32})
		.font(FONT)
		.align("center")
		.text("to Play Again!");

    bg.bind("Click", function() {
        Crafty.scene("Game");
    });
});
